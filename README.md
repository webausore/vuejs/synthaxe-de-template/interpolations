# Vue JS - Synthaxe de template - Interpolation

---

## Exercice

Preview : https://vuejs-synthaxe-de-vue-interpolation.netlify.app/

Reproduire exactement [cette page](https://vuejs-synthaxe-de-vue-interpolation.netlify.app/) en utilisant les différentes propriétés vues dans le chapitre sur l'interpolation. Ne pas hésiter a inspecter l'html.
