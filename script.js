const app = new Vue({
  el: "#app",
  data: {
    nb: 33,
    text:
      "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.",
    html: `<span style="color: red">sit aspernatur aut odit aut fugit</span>`,
    isButtonDisabled: false,
  },
  methods: {
    changeNb: function () {
      this.nb = Math.floor(Math.random() * 100);
    },
  },
});
